## Consignes

1 - Créer une base de donnée.
2 - trouver 2 sites qui ont des photos de chambery.
3 - Créer un script php qui va chercher des images / infos.   


## Etapes 

1- Trouver deux sites
 - http://www.aperorestodisco.com/bar-chambery-4580-brock-art-cafe.html   
 - https://www.tripadvisor.fr/Restaurant_Review-g8309764-d6577144-Reviews-L_Epicurial-Chambery_Savoie_Auvergne_Rhone_Alpes.html#photos;aggregationId=&albumid=101&filter=7   
2 - Trier les infos pertinentes
3 - Faire un schéma relationnel   
4 - Créer la base de données   
5 - Récupération des données (PHP)   
6 - Enregistrement, stockage dans la BDD.   

