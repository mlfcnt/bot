# Projet Bot

L’objectif est de programmer un « bot » permettant d’aller récupérer automatiquement des images sur internet en rapport avec la ville de Chambéry et de stocker les informations liées aux images dans une base de données SQLite.

Durée : 2 semaines
Groupe : projet individuel

## Objectifs :
    • Conception d’un schéma de données et manipulation des données
    • Introduction à la programmation orientée objet avec PHP
    • Appréhender le protocole HTTP via « request » et « response » (PSR7)
      
## Compétences :
    • Back-end : Créer une base de données
    • Back-end : Développer les composants d’accès aux données

### Dispositif pédagogique :
    • Veilles pédagogique sur les notions devant être abordées tout au long de la réalisation du projet
    • Suivre les étapes une par une
    • Contraintes : l’application « bot » doit fonctionner sans serveur web, l’utilisation de PDO pour la connexion et manipulation de la base de données

## Projet :

Analyse et sélection de sites web (minimum 2) dans lesquels sont mis à disposition des images et informations en rapport avec la ville de Chambéry. Il faudra ensuite modéliser un schéma de données afin de stockées les informations que le « bot » devra récupérer et insérer dans la base de données SQLite.

Composer devra être installé afin de charger les composants PHP nécessaires au fonctionnement de l’application « bot ». Les pages HTML des sites web précédemment sélectionnées devront être récupérées par l’intermédiaire du client HTTP nommé Guzzle ; l’analyse du contenu HTML sera réalisée par l’intermédiaire du composant DomCrawler (brique indépendante du framework Symfony).

Lors de l’analyse des pages HTML, le programme devra stocker les images et les informations liées dans la base de données SQLite.

## Bonus :

    • Ajouter un fichier de configuration au format YAML pour paramétrer le comportement de votre « bot ».
    • Manipuler vos images via la librairie Imagine afin de stocker des images avec une résolution de votre fois, appliquer un filtre,….
    • Exploiter le composant Console (brique indépendante du framework Symfony) afin d’améliorer l’interface en ligne de commande de votre « bot ».
    • Proposer une interface graphique permettant de visualiser rapidement les informations récupérer par l’intermédiaire de votre « bot » (utilisation d’un serveur web bien entendu autorisée car nécessaire ^^)
