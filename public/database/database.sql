-- sqlite
CREATE TABLE Image
(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    alt TEXT,
    url TEXT,
    photographe TEXT,
    hauteur INTEGER,
    largeur INTEGER,
    date DATE,
    date_text TEXT,
    description TEXT

);

CREATE TABLE Lieu (
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    titre TEXT,
    adresse TEXT,
    description TEXT,
    distance_chambery INTEGER,
    note INTEGER,
    image_id INTEGER,
    FOREIGN KEY(image_id) REFERENCES Image(id)

);