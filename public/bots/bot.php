<?php
require '../../vendor/autoload.php';
use Guzzle\Http\Client;
use Symfony\Component\DomCrawler\Crawler;
try {
    $link = new PDO('sqlite:../database/database.db');
    $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (Exception $e) {
    echo "Unable to connect";
    echo $e->getMessage();
    exit;
}

echo "Connected to the database";

//* Constante qui définit le nombre de résultats souhaités par querry
define("NBRESULTATS", 10);
//!  GUZZLE
$client = new GuzzleHttp\Client();
$res = $client->request('GET', 'http://www.cartesfrance.fr/carte-france-ville/hotel_73065_Chambery.html');
echo $res->getStatusCode();
echo $res->getHeader('content-type')[0];
$gBody = $res->getBody();

//! CRAWLER
$crawler = new Crawler((string) $gBody);

//? MAIN IMAGES (src, alt, width, height) -- XPATH marche (100 matches) -- INSERT INTO marche
// * Ecriture CSS selector
// $resultImages = $crawler
//         ->filter('.hotel-panel-photos img > :not(.miniature)')->extract(array('src', 'class', 'alt', 'width', 'height'));
//* Ecriture XPATH
$ImagesSite1 = $crawler->filterXPath('//div[contains(@class, "hotel-panel-photos")] /a/img[@width > 100]')->extract(array('src', 'alt', 'width', 'height'));

//  print_r($ImagesSite1);
// foreach (array_slice($ImagesSite1, 0, NBRESULTATS) as $image1) {
//     $url = $image1[0];
//     $alt = $image1[1];
//     $largeur = $image1[2];
//     $hauteur = $image1[3];
//     $stmt = $link->prepare("INSERT INTO Image (url, alt, largeur, hauteur) VALUES (:url, :alt, :largeur, :hauteur)");
//     $stmt->bindValue(':url', $url);
//     $stmt->bindValue(':alt', $alt);
//     $stmt->bindValue(':largeur', $largeur);
//     $stmt->bindValue(':hauteur', $hauteur);
//     $stmt->execute();
// }

//? TITLES -- XPATH marche (100 matches) -- INSERT INTO marche
$resultTitles = $crawler->filter('.hotel-panel-righter > .hotel-panel-titre > a')->extract(array('title'));
// print_r($resultTitles);

// foreach (array_slice($resultTitles, 0, NBRESULTATS) as $titres) {
//     $stmt = $link->prepare("INSERT INTO Lieu (titre) VALUES (:titre)");
//     $stmt->bindValue(':titre', $titres);
//     $stmt->execute();
// }
//? Descriptions -- marche (100 matches)
$DescriptionSite1 = $crawler->filterXPath('//div[contains(@class, "hotel-panel-desc")]/span[1]/preceding-sibling::text()[1]')->extract(['_text']);
// div[contains(@class, "hotel-panel-desc")]/i pour prendre les <i> en anglais
// var_dump ($DescriptionSite1);
// for ($i = 0; $i < NBRESULTATS; $i++) {

//         $id = $i;
//         $sql = "UPDATE Lieu SET description=? WHERE id=?";
//         $stmt = $link->prepare($sql);
//         $stmt->execute([$DescriptionSite1[$i], $i]);
//     }

//? ADRESSES -- marche et part dans la BDD
$AdressesSite1 = $crawler->filterXPath('//div[contains(@class, "hotel-panel-infos")]/b[1]/following-sibling::text()[1]')->extract(['_text']);
// print_r($AdressesSite1);

// for ($i = 0; $i < NBRESULTATS; $i++) {

//     $id = $i;
//     $sql = "UPDATE Lieu SET adresse=? WHERE id=?";
//     $stmt = $link->prepare($sql);
//     $stmt->execute([$AdressesSite1[$i], $i]);
// }

// //? Distance depuis Chambery centre -- marche (100 matches) mais ne retourne pas qu'un INT
// $resultDistance = $crawler->filterXPath('//div[contains(@class, "hotel-panel-infos")]/b[2]')->extract(['_text']);
// // foreach ($resultDistance as $distances) {

//     // $test = preg_match('!\d+!', $distances, $matches);
//     // }
//     for ($i = 0; $i < NBRESULTATS; $i++) {

//         $id = $i;
//         $sql = "UPDATE Lieu SET distance_chambery=? WHERE id=?";
//         $stmt = $link->prepare($sql);
//         $stmt->execute([preg_match('!\d+!', $resultDistance[$i], $matches[$i]), $i]);
//     }
//? Note --  marche (90 matches) /!\ Certaines locations n'ont pas de notes du coup ça casse à partir du milieu
// $resultNotes = $crawler->filterXPath('//div[contains(@class, "hotel-panel-photos")]/div/b/span')->extract(['_text']);
// foreach ($resultNotes as $notes) {

//     preg_match('!\d+!', $notes, $matches);
//     // var_dump ($matches[0]);
// }

//! DEUXIEME SITE ----------------------------------------------------------------

$client2 = new GuzzleHttp\Client();
$res2 = $client2->request('GET', 'http://www.cartesfrance.fr/carte-france-ville/photos_73065_Chambery.html');
$gBody2 = $res2->getBody();

$crawler2 = new Crawler((string) $gBody2);

//? MAIN IMAGES (src, alt, width, height) -- marche (50 matches)

$resultImages2 = $crawler2->filterXPath('//img[contains(@class, "miniature_photo")]')->extract(array('src', 'alt', 'width', 'height'));

//  var_dump($resultImages2[0][0]);
// for ($i = 0; $i < NBRESULTATS; $i++) {
//     $stmt = $link->prepare("UPDATE Image SET url=?, alt=?, largeur=?, hauteur=? WHERE id=?");
//     $stmt->execute([$resultImages2[$i][0],$resultImages2[$i][1],$resultImages2[$i][2],$resultImages2[$i][3], $i]);
// }

//? DATES PHOTOS -- marche (50 matches) mais trop d'infos. Texte + date

$resultDates = $crawler2->filterXPath('//div[contains(@class, "photo-description-inner")]/a[1]/following-sibling::text()[1]')->extract('_text');
// var_dump ($resultDates);

//? PHOTOGRAPHES -- marche (50 matches)

$resultPhotographes = $crawler2->filterXPath('//div[contains(@class, "photo-description-inner")]/a[2]')->extract('_text');
// var_dump ($resultPhotographes);

//? DESCRIPTIONS2 -- marche (50 matches)

$resultDescriptions2 = $crawler2->filterXPath('//div[contains(@class, "photo-description-inner")]/span')->extract('_text');
// var_dump ($resultDescriptions2);

//! ENVOI VERS DB!
for ($i = 0; $i < NBRESULTATS; ++$i) {
    $stmt = $link->prepare("INSERT INTO Lieu (titre, description, adresse) VALUES (:titre, :description, :adresse)");

    $stmt2 = $link->prepare("INSERT INTO Image (url, alt, largeur, hauteur, description, date_text, photographe) VALUES (:image_url, :alt, :largeur, :hauteur, :description, :date_text,:photographe)");
//? Envoi site 1 vers Lieu
    $result = $stmt->execute(array(':titre' => $resultTitles[$i], ':description' => $DescriptionSite1[$i], ':adresse' => $AdressesSite1[$i]));
//? Envoi site 1 vers Image
    $result2 = $stmt2->execute(array(':image_url' => $ImagesSite1[$i][0], ':alt' => $ImagesSite1[$i][1], ':largeur' => $ImagesSite1[$i][2], ':hauteur' => $ImagesSite1[$i][3], ':description' => $DescriptionSite1[$i]));
//? Envoi site 2 vers Lieu
    $result3 = $stmt->execute(array(':description' => $resultDescriptions2[$i]));
//? Envoi site 2 vers image
    $result4 = $stmt2->execute(array(':image_url' => $resultImages2[$i][0], ':alt' => $resultImages2[$i][1], ':largeur' => $resultImages2[$i][2], ':hauteur' => $resultImages2[$i][3], ':description' => $resultDescriptions2[$i], 'date_text'=> $resultDates[$i], ':photographe'=>$resultPhotographes[$i]));

    // var_dump($result);
}
